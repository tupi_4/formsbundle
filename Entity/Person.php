<?php

namespace Tupi\FormsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Application\Tupi\ContentBundle\Entity\Entity;



class Person extends Entity
{
  //O atributo ID está dentro da Classe entity

  /**
   * @var string
   *
   * @ORM\Column(name="name", type="string", length=250, nullable=true)
   */
  protected $name;

  /**
   * @var string
   *
   * @ORM\Column(name="email", type="string", length=250, nullable=true)
   */
  protected $email;

  /**
   * @var string
   *
   * @ORM\Column(name="phone", type="string", length=250, nullable=true)
   */
  protected $phone;

  public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}

	public function getEmail(){
		return $this->email;
	}

	public function setEmail($email){
		$this->email = $email;
	}

	public function getPhone(){
		return $this->phone;
	}

	public function setPhone($phone){
		$this->phone = $phone;
	}
}
