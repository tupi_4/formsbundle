<?php

namespace Tupi\FormsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contato
 *
 * @ORM\Table(name="bundles_forms_contato")
 * @ORM\Entity(repositoryClass="Tupi\FormsBundle\Repository\ContatoRepository")
 */

class Contato extends Person
{
  //O atributo ID está dentro da Classe entity

  /**
   * @var string
   *
   * @ORM\Column(name="subject", type="string", length=250, nullable=true)
   */
  protected $subject;

  /**
   * @var App\Application\Sonata\MediaBundle\Entity\Media
   * @ORM\ManyToOne(targetEntity="App\Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
   */
  protected $attachment;

  /**
   * @var text
   *
   * @ORM\Column(name="message", type="string", length=1000, nullable=true)
   */
  protected $message;

  /**
   * indica se a mensagem já foi lida ou não
   * @var string
   *
   * @ORM\Column(name="status", type="string", length=250, nullable=true)
   */
  protected $status = 'unread';

  /**
   * indica se a mensagem é um contato externo ou interno ao sistema
   * 
   * @ORM\Column(name="source", type="string", length=100)
   */
  protected $source = 'interno';

  /**
   * Exibe assunto para apresentação na interface gráfica,
   * evitando duplicação e descentralização do código que
   * formata o texto de apresentação
   * @return string assunto no formato "assunto (origem)"
   */
  public function getPresentationSubject() {
    $subject = $this->subject;
    if (empty($subject)) {
      $subject = "Contato - {$this->name}";
    }
    $source = $this->source;
    if ($source) {
      $source = "(". ucfirst($source) .")";
    }

    $subject .= " $source";
    return $subject;
  }

  public function getSubject(){
		return $this->subject;
	}

	public function setSubject($subject){
		$this->subject = $subject;
	}

	public function getAttachment(){
		return $this->attachment;
	}

	public function setAttachment($attachment){
    
    //$this->attachment = $attachment->getClientOriginalName();
    $this->attachment = $attachment;
	}

	public function getMessage(){
		return $this->message;
	}

	public function setMessage($message){
		$this->message = $message;
	}

  /**
   * Get indica se a mensagem já foi lida ou não
   *
   * @return  string
   */ 
  public function getStatus()
  {
    return $this->status;
  }

  /**
   * Set indica se a mensagem já foi lida ou não
   *
   * @param  string  $status  indica se a mensagem já foi lida ou não
   *
   * @return  self
   */ 
  public function setStatus($status)
  {
    $this->status = $status;

    return $this;
  }

  /**
   * Get indica se a mensagem é um contato externo ou interno ao sistema
   */ 
  public function getSource()
  {
    return $this->source;
  }

  /**
   * Set indica se a mensagem é um contato externo ou interno ao sistema
   *
   * @return  self
   */ 
  public function setSource($source)
  {
    $this->source = $source;

    return $this;
  }
}
