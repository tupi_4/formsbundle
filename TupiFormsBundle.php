<?php
namespace Tupi\FormsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TupiFormsBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'TupiFormsBundle';
    }
}