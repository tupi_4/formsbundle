<?php
namespace Tupi\FormsBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sonata\AdminBundle\Controller\CRUDController;

/**
 * Contact Admin controller.
 *
 * @Route("/admin/bundles/forms/contato")
 */
class ContatoAdminController extends CRUDController
{

    public function getBundleName()
    {
        return 'TupiFormsBundle';
    }

    public function getEntityName()
    {
        return 'Contato';
    }

    public function getFormType()
    {
        return 'Tupi\FormsBundle\Form\ContatoType';
    }
}

