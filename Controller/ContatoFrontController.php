<?php
namespace Tupi\FormsBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Contato Front controller.
 *
 * @Route("/")
 */
class ContatoFrontController extends Controller
{

    public function getBundleName()
    {
        return 'TupiFormsBundle';
    }

    public function getEntityName()
    {
        return 'Contato';
    }

    public function getFormType()
    {
        return 'Tupi\FormsBundle\Form\ContatoType';
    }

    /**
    * @Route("/contact", name="front_contact")
    */

    public function index()
    {
      return $this->render('@Tupi/FormsBundle/Resources/views/Contato/contact_front.html.twig');
    }
}

