<?php
namespace Tupi\FormsBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\MediaBundle\Form\Type\MediaType;

class ContatoAdmin extends AbstractAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('edit');        
    }
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name')
            ->add('email')
            ->add('phone')
            ->add('subject')
            ->add('attachment')
            ->add('message')
            ;
    }
    protected function configureListFields(ListMapper $listMapper)
    {

      $listMapper
                ->addIdentifier('name')
                ->add('email')
                ->add('phone')
                ->add('subject')
                ;
    }
    protected function configureFormFields(FormMapper $formMapper)
    {
      $formMapper
      ->add('name',TextType::class, array(
                'label' => false,
                'attr' => ['placeholder' => 'Nome*'],
                'required' => true
            ))
      ->add('email',EmailType::class, array(
                'label' => false,
                'attr' => ['placeholder' => 'Email*'],
                'required' => true
            ))
      ->add('phone',TextType::class, array(
                'label' => false,
                'attr' => ['placeholder' => 'Telefone*', 'class' => 'phone_with_ddd'],
                'required' => true                
            ))
      ->add('subject',TextType::class, array(
            'label' => false,
            'attr' => ['placeholder' => 'Assunto*'],
            'required' => true
            ))
      ->add('attachment',MediaType::class, array(
            'label' => 'Enviar Anexo',
            'required' => false
            ))
      ->add('message',TextareaType::class, array(
                'label' => false,
                'attr' => ['placeholder' => 'Mensagem'],
                'required' => true
            ))
        ;
    }    
}

