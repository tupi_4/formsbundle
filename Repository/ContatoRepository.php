<?php

namespace Tupi\FormsBundle\Repository;

use Doctrine\ORM\EntityManager;

use Doctrine\ORM\EntityRepository;

class ContatoRepository extends EntityRepository
{
    public function findAll()
    {
        return $this->findBy(array(), array('id' => 'DESC'));
    }

    public function findUnread()
    {
      // verificando se o status é null também para cobrir os contatos salvos
      // antes do momento em que o atributo status foi adicionado
      return  $this->_em->createQueryBuilder()
        ->select('c')
        ->from('TupiFormsBundle:Contato', 'c')                                                
        ->where('c.status IS NULL OR c.status = \'unread\'')
        ->getQuery()
        ->getResult()
      ;
    }


    public function updateStatusContact($id)
    {
        $this->_em->createQueryBuilder()
          ->update('TupiFormsBundle:Contato', 'c')
          ->set('c.status', "'read'")
          ->where('c.id = :id')
          ->setParameter(':id', $id)
          ->getQuery()
          ->getResult()
          ;
    }
    
}
